# README #
## A class for finding records in an Active Directory/LDAP server ##

This class allows you to bind to an Active Directory or LDAP server and then
find records based on search criteria. At the moment, you cannot bind to
the server anonymously. That *may* be added in the future.

## Documentation

Documentation is available at https://vespula.bitbucket.io/ldap/
