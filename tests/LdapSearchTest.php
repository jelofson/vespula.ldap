<?php
namespace Vespula\Ldap;
use Vespula\Ldap\LdapSearch;
use PHPUnit\Framework\TestCase;


final class LdapSearchTest extends TestCase 
{

    protected $uri = 'ldap.mycompany.org';
    protected $bindOptions = [
        'basedn'=>'OU=MyCompany,OU=City,OU=Province',
        'binddn'=>'cn=special,OU=MyCompany,OU=City,OU=Province',
        'bindpw'=>'bindpass',
    ];
    protected $ldapOptions = [
        'LDAP_OPTION_A'=>3,
    ];
    protected $wrapper;

    public function setUp(): void
    {
        $this->wrapper = $this->getMockBuilder('\Vespula\Ldap\LdapWrapper')
                              ->getMock();

        $this->wrapper->method('connect')->willReturn(true);
        $this->wrapper->method('bind')->willReturn(true);
    }

    public function testFind()
    {

        $sample_entry = [
            'cn'=>[
                'count'=>1,
                'juser'
            ],
            'cn',
            'sn'=>[
                'count'=>1,
                'User'
            ],
            'sn',
            'email'=>[
                'count'=>1,
                'juser@mycompany.org'
            ],
            'email',
            'count'=>3
        ];
        $user_dn = 'CN=juser,OU=MyCompany,OU=City,OU=Province';


        $this->wrapper->method('firstEntry')->willReturn(true);
        $this->wrapper->method('getAttributes')->willReturn($sample_entry);
        $this->wrapper->method('getDn')->willReturn($user_dn);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $row = $ldap->find('cn=juser');
        $expected = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'dn'=>$user_dn
        ];
        $this->assertEquals($row, $expected);
    }

    public function testFindAll()
    {
        $entries = [
            'count'=>2,
            [
                'cn'=>[
                    'count'=>1,
                    'juser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>1,
                    'juser@mycompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>[
                    'count'=>1,
                    'jdoe'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'Doe'
                ],
                'sn',
                'email'=>[
                    'count'=>2,
                    'jdoe@mycompany.org',
                    'jdoe@othercompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=jdoe,OU=MyCompany,OU=City,OU=Province'
            ]

        ];


        $this->wrapper->method('getEntries')->willReturn($entries);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $row = $ldap->findAll('cn=j*');

        $expected = [
            [
                'cn'=>'juser',
                'sn'=>'User',
                'email'=>'juser@mycompany.org',
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>'jdoe',
                'sn'=>'Doe',
                'email'=>[
                    'jdoe@mycompany.org',
                    'jdoe@othercompany.org'
                ],
                'dn'=>'CN=jdoe,OU=MyCompany,OU=City,OU=Province'
            ],
        ];
        $this->assertEquals($row, $expected);
    }

    public function testFindOneBy()
    {
        $sample_entry = [
            'cn'=>[
                'count'=>1,
                'juser'
            ],
            'cn',
            'sn'=>[
                'count'=>1,
                'User'
            ],
            'sn',
            'email'=>[
                'count'=>1,
                'juser@mycompany.org'
            ],
            'email',
            'count'=>3
        ];
        $user_dn = 'CN=juser,OU=MyCompany,OU=City,OU=Province';

        $this->wrapper->method('firstEntry')->willReturn(true);
        $this->wrapper->method('getAttributes')->willReturn($sample_entry);
        $this->wrapper->method('getDn')->willReturn($user_dn);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $row = $ldap->findOneByUserid('juser');

        $expected = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'dn'=>$user_dn
        ];

        $this->assertEquals($row, $expected);

    }

    public function testFindAllBy()
    {
        $entries = [
            'count'=>2,
            [
                'cn'=>[
                    'count'=>1,
                    'juser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>1,
                    'juser@mycompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>[
                    'count'=>1,
                    'jdoe'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'Doe'
                ],
                'sn',
                'email'=>[
                    'count'=>2,
                    'jdoe@mycompany.org',
                    'jdoe@othercompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=jdoe,OU=MyCompany,OU=City,OU=Province'
            ]

        ];


        $this->wrapper->method('getEntries')->willReturn($entries);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $row = $ldap->findAllByUserid('cn=j*');

        $expected = [
            [
                'cn'=>'juser',
                'sn'=>'User',
                'email'=>'juser@mycompany.org',
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>'jdoe',
                'sn'=>'Doe',
                'email'=>[
                    'jdoe@mycompany.org',
                    'jdoe@othercompany.org'
                ],
                'dn'=>'CN=jdoe,OU=MyCompany,OU=City,OU=Province'
            ],
        ];
        $this->assertEquals($row, $expected);
    }


    public function testSetFindByAttribute()
    {
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $ldap->setFindByAttribute('myAttrib', 'ma');

        $expected = 'ma';
        $this->assertEquals($expected, $ldap->getFindByAttribute('myAttrib'));
    }

    public function testSetFindByAttributes()
    {
        $attribs = [
            'name'=>'fn',
            'locality'=>'l'
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setFindByAttributes($attribs);

        $this->assertEquals('fn', $ldap->getFindByAttribute('name'));
        $this->assertEquals('l', $ldap->getFindByAttribute('locality'));
    }

    public function testFindOneByNewAttrib()
    {
        $sample_entry = [
            'cn'=>[
                'count'=>1,
                'juser'
            ],
            'cn',
            'sn'=>[
                'count'=>1,
                'User'
            ],
            'sn',
            'email'=>[
                'count'=>1,
                'juser@mycompany.org'
            ],
            'email',
            'count'=>3
        ];
        $user_dn = 'CN=juser,OU=MyCompany,OU=City,OU=Province';

        $this->wrapper->method('firstEntry')->willReturn(true);
        $this->wrapper->method('getAttributes')->willReturn($sample_entry);
        $this->wrapper->method('getDn')->willReturn($user_dn);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $ldap->setFindByAttribute('testAttrib', 't');
        $row = $ldap->findOneByTestAttrib('juser');

        $expected = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'dn'=>$user_dn
        ];

        $this->assertEquals($row, $expected);

    }
    
    public function testFindAllBooleanAnd()
    {
        $entries = [
            'count'=>2,
            [
                'cn'=>[
                    'count'=>1,
                    'juser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>1,
                    'juser@mycompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>[
                    'count'=>1,
                    'suser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>2,
                    'suser@mycompany.org',
                    'suser@othercompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=suser,OU=MyCompany,OU=City,OU=Province'
            ]

        ];


        $this->wrapper->method('getEntries')->willReturn($entries);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $row = $ldap->findAllByLocalityAndLastname('City', 'User');

        $expected = [
            [
                'cn'=>'juser',
                'sn'=>'User',
                'email'=>'juser@mycompany.org',
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>'suser',
                'sn'=>'User',
                'email'=>[
                    'suser@mycompany.org',
                    'suser@othercompany.org'
                ],
                'dn'=>'CN=suser,OU=MyCompany,OU=City,OU=Province'
            ],
        ];
        $this->assertEquals($row, $expected);
    }
    
    public function testFindAllBooleanOr()
    {
        $entries = [
            'count'=>2,
            [
                'cn'=>[
                    'count'=>1,
                    'juser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>1,
                    'juser@mycompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>[
                    'count'=>1,
                    'suser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>2,
                    'suser@mycompany.org',
                    'suser@othercompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=suser,OU=MyCompany,OU=City,OU=Province'
            ]

        ];


        $this->wrapper->method('getEntries')->willReturn($entries);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $row = $ldap->findAllByLocalityOrLastname('City', 'User');

        $expected = [
            [
                'cn'=>'juser',
                'sn'=>'User',
                'email'=>'juser@mycompany.org',
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>'suser',
                'sn'=>'User',
                'email'=>[
                    'suser@mycompany.org',
                    'suser@othercompany.org'
                ],
                'dn'=>'CN=suser,OU=MyCompany,OU=City,OU=Province'
            ],
        ];
        $this->assertEquals($row, $expected);
    }
    
    
    public function testIsActiveZero()
    {
        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>0
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $active = $ldap->isActive($entry);
        $this->assertTrue($active);
    }

    public function testIsActiveFalse()
    {
        $yesterday = strtotime('-1 day');
        $windowsTS = ($yesterday+11644473600)*10000000;
        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>$windowsTS
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $active = $ldap->isActive($entry);
        $this->assertFalse($active);
    }

    public function testIsActiveTrue()
    {
        $tomorrow = strtotime('+1 day');
        $windowsTS = ($tomorrow+11644473600)*10000000;
        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>$windowsTS
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $active = $ldap->isActive($entry);
        $this->assertTrue($active);
    }

    public function testIsActiveMaxInt()
    {
        // Test the max 64bit integer that is sometimes used for never expires
        $windowsTS = 9223372036854775807;
        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>$windowsTS
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $active = $ldap->isActive($entry);
        $this->assertTrue($active);
    }

    public function testSetAccountExpiresAttribute()
    {
        $tomorrow = strtotime('+1 day');
        $windowsTS = ($tomorrow+11644473600)*10000000;

        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'expires'=>$windowsTS
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setAccountExpiresAttribute('expires');

        $active = $ldap->isActive($entry);
        $this->assertTrue($active);

    }

    /*
    public function testSetAccountExpiresAttributeException()
    {
        $this->expectException(\Exception::class);
        $tomorrow = strtotime('+1 day');
        $windowsTS = ($tomorrow+11644473600)*10000000;

        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>$windowsTS
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setAccountExpiresAttribute('expires');

        $ldap->isActive($entry);
    }
    */

    public function testSetTimestampFormatWinnt()
    {
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setTimestampFormat('winnt');

        $nextweek = strtotime('+1 week');
        $windowsTS = ($nextweek+11644473600)*10000000;

        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>$windowsTS
        ];

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $active = $ldap->isActive($entry);
        $this->assertTrue($active);

        $lastweek = strtotime('-1 week');
        $windowsTS = ($lastweek+11644473600)*10000000;

        $entry['accountexpires'] = $windowsTS;

        $active = $ldap->isActive($entry);
        $this->assertFalse($active);
    }

    public function testSetTimestampFormatYmd()
    {
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setTimestampFormat('ymd');

        $nextweek = strtotime('+1 week');
        $ymdTs = gmdate('YmdHis\Z', $nextweek);

        $entry = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'accountexpires'=>$ymdTs
        ];

        $active = $ldap->isActive($entry);
        $this->assertTrue($active);

        $lastweek = strtotime('-1 week');
        $ymdTs = gmdate('YmdHis\Z', $lastweek);
        $entry['accountexpires'] = $ymdTs;

        $active = $ldap->isActive($entry);
        $this->assertFalse($active);
    }

    public function testSetTimestampFormatException()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Incorrect format. Must be winnt or ymd');

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setTimestampFormat('abc');
    }

    public function testCheckBindOptionsBaseDn()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Missing basedn in bind options');
        unset($this->bindOptions['basedn']);
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->find('blah');
    }

    public function testCheckBindOptionsBindDn()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Missing binddn in bind options');
        unset($this->bindOptions['binddn']);
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->find('blah');
    }

    public function testCheckBindOptionsBindPw()
    {
        $this->expectException(\Exception::class);
        $this->expectExceptionMessage('Missing bindpw in bind options');

        unset($this->bindOptions['bindpw']);
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->find('blah');
    }

    public function testGetWinNTTimestamp()
    {
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);

        $timestamp = 1464961968;

        $expected = 131094355680000000;

        $winnt = $ldap->getWinntTimestamp($timestamp);
        $this->assertEquals($expected, $winnt);
    }
    
    // This test is difficult because the attribs are used in LDAP search and are very hard to test if
    // They are working correctly
    public function testSetGetDefaultAttributes()
    {
        $attribs = [
            'cn',
            'sn',
            'email'
        ];
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setDefaultAttributes($attribs);
        
        $expected = $ldap->getDefaultAttributes();
        
        $this->assertEquals($expected, $attribs);
    }

    // Doesn't really do anything as sort is performed on the ldap wrapper and can't really be tested well.
    /*
    public function testSetSortBy()
    {
        $entries = [
            'count'=>2,
            [
                'cn'=>[
                    'count'=>1,
                    'juser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>1,
                    'juser@mycompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=juser,OU=MyCompany,OU=City,OU=Province'
            ],
            [
                'cn'=>[
                    'count'=>1,
                    'suser'
                ],
                'cn',
                'sn'=>[
                    'count'=>1,
                    'User'
                ],
                'sn',
                'email'=>[
                    'count'=>2,
                    'suser@mycompany.org',
                    'suser@othercompany.org'
                ],
                'email',
                'count'=>3,
                'dn'=>'CN=suser,OU=MyCompany,OU=City,OU=Province'
            ]

        ];


        $this->wrapper->method('sort')->willReturn(true);
        $this->wrapper->method('getEntries')->willReturn($entries);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setSortBy('cn');

        $this->assertEquals($ldap->getSortBy(), 'cn');

        $ldap->findAllByLocalityOrLastname('City', 'User');
        // After the find, the sortby is set back to null
        $this->assertNull($ldap->getSortBy());
        
    }
    */
    public function testFixAttribs()
    {
        $sample_entry = [
            'cn'=>[
                'count'=>1,
                'juser'
            ],
            'cn',
            'sn'=>[
                'count'=>1,
                'User'
            ],
            'sn',
            'email'=>[
                'count'=>1,
                'juser@mycompany.org'
            ],
            'email',
            'count'=>3
        ];
        $user_dn = 'CN=juser,OU=MyCompany,OU=City,OU=Province';


        $this->wrapper->method('firstEntry')->willReturn(true);
        $this->wrapper->method('getAttributes')->willReturn($sample_entry);
        $this->wrapper->method('getDn')->willReturn($user_dn);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        
        $ldap->setDefaultAttributes([
            'cn'=>'commonName',
            'sn',
            'email'
        ]);

        $row = $ldap->find('cn=juser');
        $expected = [
            'cn'=>'juser',
            'sn'=>'User',
            'email'=>'juser@mycompany.org',
            'dn'=>$user_dn
        ];
        $this->assertEquals($row, $expected);
    }
    
    public function testSetGetAttributeAliases()
    {
        $aliases = [
            'cn'=>'commonName',
            'sn'=>'surname',
        ];
        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setAttributeAliases($aliases);
        
        $expected = $ldap->getAttributeAliases();
        
        $this->assertEquals($expected, $aliases);
    }
    
    public function testAliases()
    {

        $sample_entry = [
            'cn'=>[
                'count'=>1,
                'juser'
            ],
            'cn',
            'sn'=>[
                'count'=>1,
                'User'
            ],
            'sn',
            'email'=>[
                'count'=>1,
                'juser@mycompany.org'
            ],
            'email',
            'count'=>3
        ];
        $user_dn = 'CN=juser,OU=MyCompany,OU=City,OU=Province';


        $this->wrapper->method('firstEntry')->willReturn(true);
        $this->wrapper->method('getAttributes')->willReturn($sample_entry);
        $this->wrapper->method('getDn')->willReturn($user_dn);

        $ldap = new LdapSearch($this->wrapper, $this->uri, $this->bindOptions, $this->ldapOptions);
        $ldap->setAttributeAliases([
            'sn'=>'surname',
            'email'=>'e-mail'
        ]);
        $row = $ldap->find('cn=juser');
        $expected = [
            'cn'=>'juser',
            'surname'=>'User',
            'e-mail'=>'juser@mycompany.org',
            'dn'=>$user_dn
        ];
        $this->assertEquals($row, $expected);
    }
    
}
