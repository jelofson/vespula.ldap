<?php
namespace Vespula\Ldap;

use \Vespula\Ldap\LdapWrapper;

/**
 * This class is used to bind to an Active Directory/LDAP server and search for users
 * based on various search criteria. It is NOT indended to have any update/create/delete
 * functionality.
 *
 * In order to make it testable, the php ldap function are wrapped in php methods and
 * placed in their own class. This way, that class can be mocked and this class can be tested.
 *
 * Basic Usage:
 *
 * $uri = 'myldap.company.org'
 *
 * // basedn : the dn for searching
 * // binddn : bind to the server using this (not using anonymous bind at the moment)
 * // bindpw : bind to the server using this passwd.
 *
 * $bindOptions = [
 *     'basedn'=>'ou=users,ou=mycompany,ou=org',
 *     'binddn'=>'cn=foo,ou=users,ou=mycompany,ou=org',
 *     'bindpw'=>'********',
 * ];
 *
 * // See ldap docs for these.
 * // http://php.net/manual/en/function.ldap-set-option.php
 *
 * $ldapOptions = [
 *     LDAP_OPT_PROTOCOL_VERSION=>3,
 *     LDAP_OPT_REFERRALS=>0
 * ];
 *
 * $port = 389;
 *
 * $wrapper = new \Vespula\Ldap\LdapWrapper;
 *
 * $ldap = new \Vespula\LdapSearch($wrapper, $uri, $bindOptions, $ldapOptions, $port);
 *
 * // Find one using the ldap search filter
 * $user = $ldap->find('cn=juser');
 *
 * // Find users with surname matching smit* (smith, smithers, smitt)
 * $users = $ldap->findAll('sn=smit*');
 * 
 * Other convenience methods:
 * 
 * $users = $ldap->findAllByLastnameAndLocality('User', 'City');
 * $users = $ldap->findAllByLastnameOrLocality('User', 'City');
 * $users = $ldap->findOneByLastnameAndLocality('User', 'City');
 * // Will only return the first entry found
 * $users = $ldap->findOneByLastnameOrLocality('User', 'City');
 *
 * // NOTE: Results will always contain the user dn with the return data.
 *
 * You can pass a search dn to override the base dn if you want
 * You can pass an array of attributes to return if you want.
 *
 * $attribs = [
 *     'email',
 *     'sn',
 *     'phone'
 * ];
 * $dn = 'ou=special, ou=company, ou=org';
 * $user = $ldap->find('cn=userid', $attribs, $dn);
 *
 * @author Jon Elofson <jon.elofson@gmail.com>
 *
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 *
 */
class LdapSearch {

    /**
     * An Ldap wrapper object that has ldap functions wrapped in php methods.
     *
     * @var \Vespula\Ldap\LdapWrapper
     */
    protected $wrapper;

    /**
     *
     * @var string The host URI to connect to
     */
    protected $uri;

    /**
     *
     * @var int The host port. Default is 389
     */
    protected $port;

    /**
     * `basedn` : the base dn to search in
     * `binddn` : bind to the server on this dn (no anonymous binding)
     * `bindpw` : bind to the server using this passwd
     *
     * @var array Bind options when binding the the server to search
     */
    protected $bindOptions;

    /**
     * @see http://php.net/manual/en/function.ldap-set-option.php
     *
     * @var array Various ldap options
     */
    protected $ldapOptions;

    /**
     * @see http://php.net/manual/en/function.ldap-connect.php
     *
     * @var link identifier The ldap connection link identifier
     */
    protected $conn;


    /**
     * You can sepecify special mapping of method id to attribute here. It will take advantage of the
     * __call() method to do either findOneByXyz() or findAllByXyz(). In this example, you would need a
     * key called xyx mapping to a real attribute. Then you could call $ldap->findOneByYxz('somevalue');
     *
     * @var array Mapping of special method names to actual active directory user attributes
     */
    protected $findByAttributes = [
        'lastname'=>'sn',
        'firstname'=>'givenname',
        'userid'=>'cn',
        'locality'=>'l'
    ];

    /**
     * You can check if a user account is expired based on this attribute
     *
     * @var string The AD attribute for expired account timestamps
     */
    protected $accountExpiresAttribute = 'accountexpires';

    /**
     * What timestamp format the AD is using. Allowd are winnt (default) or ymd
     *
     * winnt uses the 18 digit long windows nt format
     *
     * ymd is YYYYMMDDHHMMSST (T will typically be Z for zulu)
     *
     * @var string Timestamp format
     */
    protected $timestampFormat = 'winnt';

    /**
     * Charactes to escape in the search filters
     *
     * @var string Escape characters used in addcslashes()
     */
    protected $escapeChars = '\\&!|=<>,+-"\';()';
    
    /**
     * 
     * @var array Default attributes to return
     */
    protected $defaultAttributes = [];
    
    /**
     *
     * @var array Use these as aliases for the real attributes
     */
    protected $attributeAliases = [];
    
    /**
     * The sort attribute used before getting entries
     *
     * @var string The ldap sort attribute
     */
    protected $sortBy;


    /**
     * Constructor
     *
     * $bindOptions is an array of
     *
     * `basedn` : the dn to search
     * `binddn` : the dn to bind to
     * `bindpw` : the password to use when binding
     *
     * $ldapOptions is an array of ldap options. See the php manual at
     * http://php.net/manual/en/function.ldap-set-option.php
     *
     *
     * @param \Vespula\Ldap\LdapWrapper $wrapper Ldap function wrapper
     * @param string $uri ldap.mycompany.org
     * @param array $bindOptions
     * @param array $ldapOptions Optional LDAP options
     * @param integer $port The port number. Default 389
     * @throws Exception
     */
    public function __construct(LdapWrapper $wrapper, $uri, $bindOptions, $ldapOptions = null, $port = 389)
    {

        $this->wrapper = $wrapper;

        $this->uri = $uri;
        $this->port = (int) $port;
        $this->bindOptions = (array) $bindOptions;
        $this->ldapOptions = (array) $ldapOptions;

        $this->conn = $this->wrapper->connect($this->uri, $this->port);
        $this->wrapper->setLdapOptions($this->conn, $this->ldapOptions);
        $this->checkBindOptions($this->bindOptions);

        $bind = $this->wrapper->bind($this->conn, $this->bindOptions['binddn'], $this->bindOptions['bindpw']);
        if (! $bind) {
            throw new \Exception('Could not bind to basedn');
        }
    }
    /**
     * Destructor. Used to unbind
     *
     */
    public function __destruct()
    {
        $this->wrapper->unbind($this->conn);
    }

    /**
     * Find a single entry in the directory. Uses the ldap_first_entry function internally
     *
     * Note that returned results are cleaned up to make the result array easier to work with.
     *
     * @see http://php.net/manual/en/function.ldap-search.php
     *
     * @param string $searchfilter Will be escaped
     * @param array $attributes
     * @param string $dn
     * @param int $attrsonly
     * @param int $sizelimit
     * @param int $timelimit
     * @param int $deref
     * @return array
     * @throws \Exception
     */
    public function find(
        string $searchfilter,
        array $attributes = [],
        string $dn = null,
        int $attrsonly = 0,
        int $sizelimit = 0,
        int $timelimit = 0,
        int $deref = LDAP_DEREF_NEVER
    ): array
    {
        if (! $dn) {
            $dn = $this->bindOptions['basedn'];
        }
        
        if (! $attributes) {
            $attributes = $this->defaultAttributes;
        }

        $resource = $this->wrapper->search(
            $this->conn, 
            $dn, 
            $searchfilter, 
            $attributes, 
            $attrsonly, 
            $sizelimit, 
            $timelimit, 
            $deref
        );

        if ($resource === false) {
            throw new \Exception('The LDAP DN search failed');
        }

        $entry = $this->wrapper->firstEntry($this->conn, $resource);

        if ($entry) {
            $data = $this->wrapper->getAttributes($this->conn, $entry);
            $data = array_change_key_case($data, CASE_LOWER);
            $data['dn'] = $this->wrapper->getDn($this->conn, $entry);
            return $this->fixAttributeValues($data);
        }

        return [];
    }

    /**
     * Find all records matching the searchfilter
     *
     * Note that returned results are cleaned up to make the result array easier to work with.
     *
     * @param string $searchfilter
     * @param array $attributes
     * @param string $dn
     * @param int $attrsonly
     * @param int $sizelimit
     * @param int $timelimit
     * @param int $deref
     * @param array $controls
     * @return array
     * @throws \Exception
     */
    public function findAll(
        $searchfilter,
        $attributes = [],
        $dn = null,
        $attrsonly = 0,
        $sizelimit = 0,
        $timelimit = 0,
        $deref = LDAP_DEREF_NEVER,
        ?array $controls = []
    ): array
    {
        if (! $dn) {
            $dn = $this->bindOptions['basedn'];
        }
        
        if (! $attributes) {
            $attributes = $this->defaultAttributes;
        }

        if ($controls) {
            $controls = $this->addControlKeys($controls);
        }

        $data = [];
        $pages = false;
        $error_message = null;
        $response_controls = [];

        do {
            $resource = $this->wrapper->search(
                $this->conn, 
                $dn, 
                $searchfilter, 
                $attributes, 
                $attrsonly, 
                $sizelimit, 
                $timelimit, 
                $deref, 
                $controls
            );

            
            if ($resource === false) {
                throw new \Exception('The LDAP DN search failed');
            }
            
    
            $this->wrapper->parseResult($this->conn, $resource, $error_message, $response_controls);
    
            // Clean up the results to make them user friendly.
            $entries = $this->wrapper->getEntries($this->conn, $resource);
            if (! $entries) {
                return [];
            }
    
            // remove 'count' from array;
            unset($entries['count']);
            foreach ($entries as $entry) {
                $data[] = $this->fixAttributeValues($entry);
            }

            $pages = false;
            $cookie = $response_controls[LDAP_CONTROL_PAGEDRESULTS]['value']['cookie'] ?? false;
            
            if ($cookie) {
                $pages = true;
                $controls[LDAP_CONTROL_PAGEDRESULTS]['value']['cookie'] = $cookie;
            } 

        } while ($pages);


        return $data;

    }
    
    /**
     * Get the ldap connection 
     * 
     * @return resource Link identifier
     */
    public function getConn()
    {
        return $this->conn;
    }

    

    /**
     * Magic method to allow for findOneBy() and findAllBy() methods.
     * You can also use findAllByAttributeAndOtherAttribute() or
     * findAllByAttributeOrOtherAttribute() so long as the attributes are keyed
     * in the findByAttributes array.
     *
     * The allowed methods are based on the findByAttributes array
     *
     * @param string $name
     * @param array $arguments
     * @return array
     * @throws \InvalidArgumentException
     */
    public function __call($name, $arguments)
    {
        $findOneBy = strpos($name, 'findOneBy');
        $findAllBy = strpos($name, 'findAllBy');
        if ($findOneBy !== false) {
            $findBy = lcfirst(str_replace('findOneBy', '', $name));

            $operator = [];
            if (preg_match("/And|Or/", $findBy, $operator)) {
                return $this->booleanSearch('find', $operator[0], $findBy, $arguments);
            }

            if (array_key_exists($findBy, $this->findByAttributes)) {
                $findBy = $this->findByAttributes[$findBy];
            }
            $value = array_shift($arguments);
            $value = addcslashes($value, $this->escapeChars);
            $searchfilter = "$findBy=$value";
            array_unshift($arguments, $searchfilter);
            
            return call_user_func_array([$this, 'find'], $arguments);
        }

        if ($findAllBy !== false) {
            $findBy = lcfirst(str_replace('findAllBy', '', $name));

            $operator = [];
            if (preg_match("/And|Or/", $findBy, $operator)) {
                return $this->booleanSearch('findAll', $operator[0], $findBy, $arguments);
            }

            if (array_key_exists($findBy, $this->findByAttributes)) {
                $findBy = $this->findByAttributes[$findBy];
            }
            $value = array_shift($arguments);
            $value = addcslashes($value, $this->escapeChars);
            $searchfilter = "$findBy=$value";
            array_unshift($arguments, $searchfilter);
            return call_user_func_array([$this, 'findAll'], $arguments);

        }

        throw new \InvalidArgumentException();

    }
    
    /**
     * Perform a boolean search via And or Or
     * 
     * @param string $findMethod Either findAll or findOne
     * @param string $operator Or or And
     * @param string $findBy The method called minus findOneBy or findAllBy (LastnameAndFirstname)
     * @param array $args The args passed to the __call method, including the values to search for
     * @return array The results
     * @throws \InvalidArgumentException
     */
    protected function booleanSearch($findMethod, $operator, $findBy, $args)
    {
        $shorthandOperators = [
            'Or'=>'|',
            'And'=>'&'
        ];

        $findMethods = ['findAll', 'find'];
        if (! in_array($findMethod, $findMethods)) {
            throw new \InvalidArgumentException('The findMethod argument must be `findAll` or `find`.');
        }


        $op = $shorthandOperators[$operator];
        $matches = [];
        preg_match("/([A-Za-z]+)$operator([A-Za-z]+)/", $findBy, $matches);
        if ($matches) {
            array_shift($matches);
            $filter = '';

            foreach ($matches as $key=>$attrib) {
                $attribKey = lcfirst($attrib);
                $value = addcslashes($args[$key], $this->escapeChars);
                if (! array_key_exists($attribKey, $this->findByAttributes)) {
                    $filter .= "($attribKey=$value)";
                } else {
                    $filter .= "({$this->findByAttributes[$attribKey]}=$value)";
                }
                
                
            }
            $searchfilter = '(' . $op . $filter . ')';
            $arguments = array_slice($args, count($matches));
            array_unshift($arguments, $searchfilter);
            return call_user_func_array([$this, $findMethod], $arguments);
        }

        throw new \InvalidArgumentException('Invalid Attribute');
    }


    /**
     * Set a findByAttribute key value pair. To be used in a findOneByXyz() or
     * findAllByXyz() call.
     *
     * @param string $attribute
     * @param string $value
     */
    public function setFindByAttribute($attribute, $value)
    {
        $this->findByAttributes[$attribute] = $value;
    }

    /**
     * Set all the findByAttributes
     *
     * @param array $attributes
     */
    public function setFindByAttributes(array $attributes)
    {
        $this->findByAttributes = $attributes;
    }

    /**
     * Get a findByAttribute by key
     *
     * @param string $attribute The attribue key
     * @return string The attribute value
     * @throws Exception
     */
    public function getFindByAttribute($attribute)
    {
        if (! isset($this->findByAttributes[$attribute])) {
            throw new \Exception('Attribute not found ' . $attribute);
        }

        return $this->findByAttributes[$attribute];
    }

    /**
     * Check to see if the user is active or not. The entry must have the
     * accountExpiresAttribute in the array. This is only useful if the active directory uses
     * an attribute to record the expiry timestamp.
     *
     * Use the entry that comes back from a find or find* method. The entry should have a element like
     * 'accountexpires'=>128699928000000000 (or a ymd format)
     *
     * @param array $entry
     * @return boolean
     * @throws Exception
     */
    public function isActive(array $entry)
    {
        if (! isset($entry[$this->accountExpiresAttribute])) {
            return false;
            //throw new \Exception('Missing account expires attribute in entry. You must have the ' .
            //    $this->accountExpiresAttribute . ' attribute in the entry array'
            //);
        }

        $timestamp = $entry[$this->accountExpiresAttribute];

        if ($timestamp == 0) {
            return true;
        }

        // Maximum integer on 64 bit arch. I see this on some accounts marked as
        // Expire = never
        
        if ($timestamp == PHP_INT_MAX) {
            return true;
        }

        switch ($this->timestampFormat) {
            case 'winnt' :
                $timestamp = ($timestamp/10000000) - 11644473600;
            break;
            case 'ymd' :
                $timestamp = strtotime($timestamp);
            break;
            default :
                throw new \Exception('Invalid timestamp format');
        }
        $timestamp = (int) $timestamp;

        if ($timestamp - time() <= 0) {
            return false;
        }

        return true;
    }

    /**
     * Set the attribute for accountExpiry used for checking isActive()
     *
     * @param string $attribute
     */
    public function setAccountExpiresAttribute($attribute)
    {
        $this->accountExpiresAttribute = $attribute;

    }
    /**
     * Get the account expires attribute
     * 
     * @return string
     */
    public function getAccountExpiresAttribute()
    {
        return $this->accountExpiresAttribute;
    }

    /**
     * Set the escape chars used in the search filter. Uses addcslashes()
     *
     * @param string $chars
     */
    public function setEscapeChars($chars = null)
    {
        $this->escapeChars = $chars;
    }
    
    /**
     * Get the escape chars used in the search filter. 
     *
     * @return string $chars
     */
    public function getEscapeChars()
    {
        return $this->escapeChars;
    }

    /**
     * Set the timestamp format to use when checking isActive()
     *
     * Only `winnt` and `ymd` are allowed
     *
     * @param string $format
     * @throws \Exception
     */
    public function setTimestampFormat($format)
    {
        $allowed = [
            'winnt',
            'ymd'
        ];

        if (!in_array($format, $allowed)) {
            throw new \Exception('Incorrect format. Must be winnt or ymd');
        }

        $this->timestampFormat = $format;
    }

    /**
     * Will convert a unix timestamp into a WindowsNT timestamp
     *
     * The return will either be an int (64bit) or float (32bit)
     *
     * @param type $timestamp
     * @return int|float The 18-digit windows NT timestamp
     */
    public function getWinntTimestamp($timestamp = null)
    {
        if (! $timestamp) {
            $timestamp = time();
        }

        $timestamp += 11644473600;
        $timestamp *= 10000000;

        return $timestamp;
    }
    
    /**
     * Set default attributes to return, if you don't want to always return all of them
     * 
     * Also useful if you don't want to pass attribs to the find methods.
     * 
     * @param array $attributes
     */
    public function setDefaultAttributes(array $attributes)
    {
        $this->defaultAttributes = $attributes;
    }
    
    /**
     * Get the default attributes array
     * 
     * @return array The default attributes returned from ldap_search
     */
    public function getDefaultAttributes()
    {
        return $this->defaultAttributes;
    }
    
    /**
     * Set attribute aliases to return instead of the default attribute name.
     * 
     * @param array $aliases
     */
    public function setAttributeAliases(array $aliases)
    {
        $this->attributeAliases = $aliases;
    }
    
    /**
     * Get the attribute alias array
     * 
     * @return array The attribute aliases
     */
    public function getAttributeAliases()
    {
        return $this->attributeAliases;
    }
    
    /**
     * Set the sort by attribute prior to finding all entries. This must be set prior to the findAllBy() or findAll() method.
     * 
     * Also, this is reset after each findAll, so you will have to set it each time. This prevents unpredicted results.
     * 
     * @param string $sortby
     */
    public function setSortBy($sortby)
    {
        $this->sortBy = $sortby;
    }

    /**
     * Get the sort by attribute
     *
     * 
     * @return string The sort by var
     */
    public function getSortBy()
    {
        return $this->sortBy;
    }

    /**
     * Cleans up the result array from an ldap search, getting rid of non-string
     * keys and the `count` key
     *
     * @param array $values
     * @return array
     */
    protected function fixAttributeValues($values)
    {
        $data = [];
        unset($values['count']);
        foreach ($values as $key=>$value) {
            if (is_string($key)) {
                if (array_key_exists($key, $this->attributeAliases)) {
                    $data[$this->attributeAliases[$key]] = $this->getAttributeValue($value);
                    continue;
                }
                $data[$key] = $this->getAttributeValue($value);
            }
        }
        return $data;
    }


    /**
     * Get a single attribute value or values depending on the count.
     *
     * @param array $value
     * @return array|string
     */
    protected function getAttributeValue($value)
    {
        if (! isset($value['count'])) {
            return $value;
        }
        if ($value['count'] == 1) {
            return $value[0];
        }
        
        unset($value['count']);
        return $value;

    }


    /**
     * Ensure bind options are set properly
     *
     * @param array $bindOptions
     * @throws Exception
     */
    protected function checkBindOptions($bindOptions)
    {
        if (! array_key_exists('basedn', $bindOptions)) {
            throw new \Exception('Missing basedn in bind options');
        }
        if (! array_key_exists('binddn', $bindOptions)) {
            throw new \Exception('Missing binddn in bind options');
        }
        if (! array_key_exists('bindpw', $bindOptions)) {
            throw new \Exception('Missing bindpw in bind options');
        }
    }

    protected function addControlKeys($controls)
    {
        $values = [];
        foreach ($controls as $controlset) {
            $oid = $controlset['oid'];
            $values[$oid] = $controlset;
        }

        return $values;
    }

}
