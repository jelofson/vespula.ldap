<?php
namespace Vespula\Ldap;


/**
 * This class is simply a wrapper around native php ldap functions making it easy 
 * to mock this class and make other classes that rely on ldap functions testable.
 * 
 * (c) Jon Elofson <jon.elofson@gmail.com>
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 * 
 */

class LdapWrapper {

    /**
     * Constructor. Ensures the ldap extension is loaded
     * 
     * @throws \Exception
     */
    public function __construct()
    {
        if (! extension_loaded('ldap')) {
            throw new \Exception('LDAP extension not loaded');
        }
    }
    /**
     * Set ldap options after connecting.
     * 
     * @param resource $conn LDAP connection
     * @param array $options Options
     */
    public function setLdapOptions($conn, $options)
    {
        foreach ($options as $option=>$value) {
            ldap_set_option($conn, $option, $value);
        }
    }
    
    
    /**
     * Connect to ldap server
     * 
     * @param string $uri
     * @param int $port
     * @return resource Link identifier
     * @throws Exception
     */
    public function connect($uri, $port)
    {
        $conn = ldap_connect($uri, $port);
        if (! $conn) {
            throw new \Exception('EXCEPTION_LDAP_CONNECT_FAILED');
        }
        return $conn;
    }
    
    /**
     * Bind to an ldap server
     * 
     * @param resource $conn Link identifier
     * @param string $dn
     * @param string $password
     * @return resource
     */
    public function bind($conn, $dn, $password)
    {
        return ldap_bind($conn, $dn, $password);
    }
    
    /**
     * Bind to and ldap server but suppress warnings. 
     * Allow this to happen because we don't care (ever) if a bind
     * failed, especially due to a bad username and password. Normally, we don't
     * want to suppress thses and use ini settings to do that, 
     * but in this case, it's ok.
     * 
     * @param resource $conn Link identifier
     * @param string $dn
     * @param string $password
     * @return resource
     */
    public function bindQuietly($conn, $dn, $password)
    {
        return @ldap_bind($conn, $dn, $password);
    }
    
    /**
     * Search the base dn for a user matching the search filter. This should be 
     * unique to the user and must match by the user's userid passed to 
     * authenticate
     * 
     * @param resource $conn Link identifier
     * @param string $basedn The base dn to search
     * @param string $searchfilter Eg. samaccountname=%s
     * @param array $attributes
     * @param int $attrsonly
     * @param int $sizelimit
     * @param int $timelimit
     * @param int $deref
     * @param array $controls LDAP Controls
     * @return resource
     */
    public function search(
        $conn, 
        string $basedn, 
        string $searchfilter, 
        array $attributes = [], 
        int $attrsonly = 0, 
        int $sizelimit = 0, 
        int $timelimit = 0, 
        int $deref = LDAP_DEREF_NEVER, 
        ?array $controls = []
    )
    {
        return ldap_search($conn, $basedn, $searchfilter, $attributes, $attrsonly, $sizelimit, $timelimit, $deref, $controls);
    }
    
    /**
     * Get values from the directory keyed by $attrib
     * 
     * @see firstEntry()
     * 
     * @param resource $conn Link identifier
     * @param resource $entry An entry from the directory
     * @param string $attrib
     * @return array
     */
    public function getValues($conn, $entry, $attrib)
    {
        return ldap_get_values($conn, $entry, $attrib);
    }
    
    /**
     * Get all attributes for a given entry
     * 
     * @param resource $conn Link identifier
     * @param resource $entry Result identifier
     * @return array
     */
    public function getAttributes($conn, $entry)
    {
        return ldap_get_attributes($conn, $entry);
    }
    
    /**
     * Get the first entry from a search result
     * 
     * @param resource $conn Link identifier
     * @param resource $resource Result identifier
     * @return resource
     */
    public function firstEntry($conn, $resource)
    {
        return ldap_first_entry($conn, $resource);
    }
    
    /**
     * Get all entries from a search result
     * 
     * @param resource $conn Link identifier
     * @param resource $resource Result identifier
     * @return array
     */
    public function getEntries($conn, $resource)
    {
        return ldap_get_entries($conn, $resource);
    }
    
    /**
     * Get the DN from a returned result
     * 
     * @param resource $conn Link identifier
     * @param  $first The first entry returned
     * @return string
     */
    public function getDn($conn, $first)
    {
        return ldap_get_dn($conn, $first);
    }
    
    /**
     * Get the DN parts
     * 
     * @param string $dn
     * @param int $with_attrib
     * @return array
     */
    public function explodeDn($dn, $with_attrib)
    {
        return ldap_explode_dn($dn, $with_attrib);
    }
    
    /**
     * Set the ldap sorting
     * 
     * @param resource $conn Link identifier 
     * @param $resource Result identifier
     * @param string $sortfilter
     * @return boolean
     */
    /*
    public function sort($conn, $resource, $sortfilter)
    {
        return ldap_sort($conn, $resource, $sortfilter);
    }
    */
    
    /**
     * Disconnect from the server
     * 
     * @param resource $conn Link identifier
     * @return boolean
     */
    public function unbind($conn)
    {
        return ldap_unbind($conn);
    }

    public function parseResult(
        $conn, 
        $result,
        string &$error_message = null, 
        array &$controls = []
    ): bool
    {
        $referrals = $matched_dn = null;
        return ldap_parse_result($conn, $result, $error_code, $matched_dn, $error_message, $referrals, $controls);
    }

}